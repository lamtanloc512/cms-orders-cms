(function ($) {
  'use strict';
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  const gBASE_URL = 'http://42.115.221.44:8080/devcamp-pizza365/orders';

  let gOrderDb = {
    orders: [],
    order: {},
    drinkOpt: [],
    filterOrders: function (paramFilterObj) {
      'use strict';
      let vOrderFilterResult = [];
      vOrderFilterResult = this.orders.filter(function (paramOrderArr) {
        if (
          paramFilterObj.trangThai == 'none' &&
          paramFilterObj.loaiPizza != 'none' &&
          paramOrderArr.loaiPizza != null
        ) {
          return (
            paramOrderArr.loaiPizza.toLowerCase() ==
            paramFilterObj.loaiPizza.toLowerCase()
          );
        } else if (
          paramFilterObj.trangThai != 'none' &&
          paramFilterObj.loaiPizza == 'none' &&
          paramOrderArr.trangThai != null
        ) {
          return paramOrderArr.trangThai.includes(paramFilterObj.trangThai);
        } else if (
          paramFilterObj.trangThai != 'none' &&
          paramFilterObj.loaiPizza != 'none' &&
          paramOrderArr.trangThai != null &&
          paramOrderArr.loaiPizza != null
        ) {
          return (
            paramOrderArr.loaiPizza.includes(paramFilterObj.loaiPizza) &&
            paramOrderArr.trangThai.includes(paramFilterObj.trangThai)
          );
        } else if (
          paramFilterObj.trangThai == 'none' &&
          paramFilterObj.loaiPizza == 'none'
        ) {
          return paramOrderArr;
        }
      });
      return vOrderFilterResult;
    },
  };

  const gCOLUMN_ID = 0;
  const gCOLUMN_KICH_CO = 1;
  const gCOLUMN_PIZZA_TYPES = 2;
  const gCOLUMN_DRINKS = 3;
  const gCOLUMN_TOTAL = 4;
  const gCOLUMN_HOVATEN = 5;
  const gCOLUMN_PHONE = 6;
  const gCOLUMN_STATUS = 7;
  const gCOLUMN_ACTION = 8;

  const gORDERS_COL = [
    'orderId',
    'kichCo',
    'loaiPizza',
    'idLoaiNuocUong',
    'thanhTien',
    'hoTen',
    'soDienThoai',
    'trangThai',
    'action',
  ];
  // định nghĩa table  - chưa có data
  let gOrderTable = $('#order-table').DataTable({
    ordering: false,
    // Khai báo các cột của datatable
    columns: [
      { data: gORDERS_COL[gCOLUMN_ID] },
      { data: gORDERS_COL[gCOLUMN_KICH_CO] },
      { data: gORDERS_COL[gCOLUMN_PIZZA_TYPES] },
      { data: gORDERS_COL[gCOLUMN_DRINKS] },
      { data: gORDERS_COL[gCOLUMN_TOTAL] },
      { data: gORDERS_COL[gCOLUMN_HOVATEN] },
      { data: gORDERS_COL[gCOLUMN_PHONE] },
      { data: gORDERS_COL[gCOLUMN_STATUS] },
      { data: gORDERS_COL[gCOLUMN_ACTION] },
    ],
    // Ghi đè nội dung của cột action, chuyển thành button chi tiết
    columnDefs: [
      {
        targets: gCOLUMN_ACTION,
        defaultContent: `<button class='info-order btn btn-primary btn-xs mx-1'><i class="fa fa-pencil-square-o mx-1"></i>Detail/Edit</button>
        <button class="btn-delete-order btn btn-danger btn-xs mx-1" type="button"><i class="fa fa-times-circle mx-1"></i>Delete</button>`,
      },
    ],
  });

  let gOrderInfo = {
    id: '',
    orderId: '',
  };

  let gCurrentRowOnDelete = null;
  let gCurrentRowOnEdit = null;
  let gOnce = true;

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  $(document).on('hidden.bs.toast', function () {
    $('.toast-container').empty();
  });

  //su dung select2
  $('.select2').select2();

  //ham xu ly su kien khi add new order
  $('#btn-addnew-order').click(function () {
    gOnce = true;
    $('#div-order-new').modal('show');
  });
  //ham xy ly su kien khi an save addnew
  $('#btn-addnew-save').click(function () {
    onBtnAddCreateNewOrder();
  });

  // gán click event handler cho button edit
  $('#order-table').on('click', '.info-order', function () {
    $('#div-order-edit').modal('show');
    onButtonChiTietClick(this); // this là button được ấn
  });

  //remder lai table khi close modal edit
  $('#div-order-edit').on('hidden.bs.modal', function () {
    sendRequestToGetAllOrder();
    loadDataToTable(gOrderDb.orders);
  });

  $('#btn-filter-order').on('click', function () {
    'use strict';
    onBtnFilterOrderClick();
  });

  // thuc thi khi an nut delete o cot action
  $('#order-table').on('click', '.btn-delete-order', function () {
    // show modal
    $('#div-modal-delete-order').modal('show');
    onBtnDeleteActionClick(this);
  });
  // thuc thi ham an nut confirm delete order
  $('#btn-ondelete').on('click', function () {
    onBtnConfirmDeleteClick(this);
  });

  $('#btn-onedit-save').on('click', (e) => {
    e.preventDefault();
    onBtnConfirmClick();
  });

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  //khai bao ham khi an nut add new
  function onBtnAddCreateNewOrder() {
    'use strict';
    //B1: get value from form
    let vAddNewFormData = getValueFormAddNew();
    let vIsValid = validate(vAddNewFormData);
    if (vIsValid && vAddNewFormData.idVourcher != '') {
      // thuc hien check voucherId
      let vVoucherAfterChecked = sendRequesetToGetVoucherCode(
        vAddNewFormData.idVourcher
      );
      if (
        vVoucherAfterChecked.phanTramGiamGia != '0' &&
        vVoucherAfterChecked.maVoucher != ''
      ) {
        swal('Hoàn hảo!', 'Áp dụng mã giảm giá thành công!', 'success').then(
          function () {
            sendDataToCreateNewOrder(vAddNewFormData);
          }
        );
      } else if (
        vVoucherAfterChecked.phanTramGiamGia == '0' &&
        vVoucherAfterChecked.maVoucher == ''
      ) {
        swal({
          title: 'Mã giảm giá không tồn tại',
          text: 'Bạn có muốn tiếp tục tạo đơn hàng không?',
          icon: 'warning',
          buttons: true,
          dangerMode: true,
        }).then((vWllDelete) => {
          if (vWllDelete) {
            sendDataToCreateNewOrder(vAddNewFormData);
          }
        });
      }
    } else if (vIsValid && vAddNewFormData.idVourcher == '') {
      sendDataToCreateNewOrder(vAddNewFormData);
    }
  }

  //khai bao ham xy ly khi an nut confirm
  function onBtnConfirmClick() {
    'use strict';
    let vStatus = $('#select-edit-status').val();
    let vId = $('#btn-onedit-save').attr('data-id');
    senDataToUpdateOrderInfo(vStatus, vId);
  }

  // infoFunction sẽ là function các nút cùng gọi
  function onButtonChiTietClick(paramChiTietButton) {
    'use strict';
    //Xác định thẻ tr là cha của nút được chọn
    let vRowSelected = $(paramChiTietButton).parents('tr');
    gCurrentRowOnEdit = vRowSelected;
    //Lấy datatable row
    let vDatatableRow = gOrderTable.row(vRowSelected);
    //Lấy data của dòng
    let vUserData = vDatatableRow.data();
    gOrderInfo.id = vUserData.id;
    gOrderInfo.orderId = vUserData.orderId;
    loadDataToModalEdit(gOrderInfo);
  }

  // hàm chạy khi trang được load
  function onPageLoading() {
    'use strict';
    // lấy data từ server
    sendRequestToGetAllOrder();
    // load drink list option vao select drink o add new modal
    sendRequestToGetDrinkOpts();
    loadDataToTable(gOrderDb.orders);
  }

  // hàm xử lý sự kiện filter users
  function onBtnFilterOrderClick() {
    'use strict';
    // Khai báo đối tượng chứa dữ liệu lọc trên form
    var vOrderFilterDataObj = {
      trangThai: '',
      loaiPizza: '',
    };
    // B1: Thu thập dữ liệu
    getFilterData(vOrderFilterDataObj);
    // B2: Validate (ko cần)
    // B3: Thực hiện nghiệp vụ lọc
    var vOrderFilterResult = gOrderDb.filterOrders(vOrderFilterDataObj);
    // B4: Hiển thị dữ liệu lên table
    loadDataToTable(vOrderFilterResult);
  }

  // khai bao ham xu ly khi an nut delete action
  function onBtnDeleteActionClick(paramElement) {
    //get id order
    'use strict';
    //Xác định thẻ tr là cha của nút được chọn
    let vRowDeleteSelected = $(paramElement).parents('tr');
    gCurrentRowOnDelete = vRowDeleteSelected;
    //Lấy datatable row
    let vRowData = gOrderTable.row(vRowDeleteSelected);
    //Lấy data của dòng
    let vUserData = vRowData.data();
    //gán data id vào nút
    $('#btn-ondelete').attr('data-id', vUserData.id);
  }

  // khai bao ham xu ly khi an nut confirm xoa
  function onBtnConfirmDeleteClick(paramElement) {
    'use strict';
    const vORDERS_URL = 'http://42.115.221.44:8080/devcamp-pizza365/orders/';
    let vIdOrder = paramElement.dataset.id;
    gCurrentRowOnDelete.css({ filter: 'contrast(0.5)' });
    $.ajax({
      type: 'DELETE',
      url: vORDERS_URL + vIdOrder,
      async: false,
      dataType: 'json',
      success: function (response) {
        $('#div-modal-delete-order').modal('hide');
        swal('Done!', 'Đã xóa thành công', 'info').then(function () {
          sendRequestToGetAllOrder();
          setTimeout(function () {
            $('.loading-screen').fadeOut(300);
            loadDataToTable(gOrderDb.orders);
          }, 500);
        });
      },
      error: function (error) {
        console.assert(error.responseText);
        swal('Opps!', 'Xóa không thành công!', 'error').then(function () {
          sendRequestToGetAllOrder();
          setTimeout(function () {
            $('.loading-screen').fadeOut(300);
            loadDataToTable(gOrderDb.orders);
          }, 500);
        });
      },
      statusCode: {
        200: function (response) {
          console.assert(response.responseText);
        },
      },
    });
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
  //get value modal addnew
  function getValueFormAddNew() {
    'use strict';
    let vSizeValue = '';
    $.each($("input[name='radio-addnew-size']:checked"), function () {
      vSizeValue = $(this).val();
    });

    let vFormObj = {
      kichCo: '',
      duongKinh: '',
      suon: '',
      salad: '',
      idVourcher: $('#input-addnew-voucher').val(),
      idLoaiNuocUong: $('#select-addnew-drink').val(),
      soLuongNuoc: '',
      thanhTien: '',
      loaiPizza: $('#select-addnew-pizza-type').val(),
      hoTen: $('#input-addnew-fullname').val().trim(),
      email: $('#input-addnew-email').val().trim(),
      soDienThoai: $('#input-addnew-phone').val().trim(),
      diaChi: $('#input-addnew-address').val(),
      loiNhan: $('#input-addnew-message').val(),
    };

    switch (vSizeValue) {
      case 'size-s':
        vFormObj.kichCo = 'S';
        vFormObj.duongKinh = 20;
        vFormObj.suon = 2;
        vFormObj.salad = 200;
        vFormObj.soLuongNuoc = 2;
        vFormObj.thanhTien = 150000;
        break;
      case 'size-m':
        vFormObj.kichCo = 'M';
        vFormObj.duongKinh = 25;
        vFormObj.suon = 4;
        vFormObj.salad = 300;
        vFormObj.soLuongNuoc = 3;
        vFormObj.thanhTien = 200000;
        break;
      case 'size-l':
        vFormObj.kichCo = 'L';
        vFormObj.duongKinh = 30;
        vFormObj.suon = 8;
        vFormObj.salad = 500;
        vFormObj.soLuongNuoc = 4;
        vFormObj.thanhTien = 250000;
        break;
    }
    return vFormObj;
  }

  // load data to table
  function loadDataToTable(paramResponseObject) {
    'use strict';
    //Xóa toàn bộ dữ liệu đang có của bảng
    gOrderTable.clear();
    //Cập nhật data cho bảng
    gOrderTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gOrderTable.draw();
  }

  // hàm thu thập dữ liệu lọc trên form
  function getFilterData(paramFilterObj) {
    'use strict';
    paramFilterObj.trangThai = $('#input-order-status').val();
    paramFilterObj.loaiPizza = $('#input-pizza-types').val();
  }

  // hàm load data lên modal
  function loadDataToModalEdit(paramData) {
    'use strict';
    if (paramData.orderId != '') {
      let vOrderDetail = sendParamToGetOrderDetail(paramData.orderId);
      $('#input-edit-name').val(vOrderDetail.hoTen);
      $('#input-edit-email').val(vOrderDetail.email);
      $('#input-edit-phone').val(vOrderDetail.soDienThoai);
      $('#input-edit-address').val(vOrderDetail.diaChi);
      $('#textarea-msg').val(vOrderDetail.loiNhan);
      $('#select-edit-status').val(vOrderDetail.trangThai).change();
      $('#input-edit-percent').val(vOrderDetail.giamGia);
      if (vOrderDetail.giamGia != 0) {
        let vTotal = vOrderDetail.thanhTien;
        vOrderDetail.thanhTien = vTotal - vOrderDetail.giamGia;
        $('#input-edit-total').val(vOrderDetail.thanhTien);
      } else if (vOrderDetail.giamGia == 0) {
        $('#input-edit-total').val(vOrderDetail.thanhTien);
      }
      let vDrinkName = '';
      gOrderDb.drinkOpt.map((bI) => {
        if (bI.maNuocUong == vOrderDetail.idLoaiNuocUong) {
          vDrinkName = bI.tenNuocUong;
        }
      });

      let vTableInfoOrder = `
      <tbody>
        <tr>
          <td>Mã đơn hàng</td>
          <td>${vOrderDetail.orderId}</td>
        </tr>
        <tr>
          <td>Size</td>
          <td>${vOrderDetail.kichCo}</td>
        </tr>
        <tr>
          <td>Đường kính</td>
          <td>${vOrderDetail.duongKinh}cm</td>
        </tr>
        <tr>
          <td>Loại Pizza</td>
          <td>${vOrderDetail.loaiPizza}</td>
        </tr>
        <tr>
          <td>Sườn</td>
          <td>${vOrderDetail.suon} miếng</td>
        </tr>
        <tr>
          <td>Salad</td>
          <td>${vOrderDetail.salad} gram</td>
        </tr>
        <tr>
          <td>Loại nước uống</td>
          <td>${vDrinkName}</td>
        </tr>
        <tr>
          <td>Số lượng nước</td>
          <td>${vOrderDetail.soLuongNuoc} lon</td>
        </tr>
        <tr>
          <td>Mã voucher</td>
          <td>${
            vOrderDetail.giamGia != 0
              ? vOrderDetail.idVourcher + ' ' + '(Hợp lệ)'
              : vOrderDetail.idVourcher + ' ' + '(Không hợp lệ)'
          }</td>
        </tr>
        
      </tbody>`;
      $('#table-info-order').empty();
      $('#table-info-order').append(vTableInfoOrder);

      $('#btn-onedit-save').attr('data-id', paramData.id);
    }
  }

  function sendRequestToGetAllOrder() {
    'use strict';
    $.ajax({
      beforeSend: function () {
        $('.loading-screen').fadeIn(300);
      },
      url: gBASE_URL,
      type: 'GET',
      dataType: 'json',
      async: false,
      success: function (responseObject) {
        let vResultOrders = responseObject;
        vResultOrders.map((bI) => {
          bI.thanhTien = bI.thanhTien - bI.giamGia;
        });
        gOrderDb.orders = vResultOrders;
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    }).done(() => {
      setTimeout(function () {
        $('.loading-screen').fadeOut(300);
      }, 500);
    });
  }

  //khai bao ham send data de create order
  function sendDataToCreateNewOrder(paramData) {
    'use strict';
    const vORDERS_URL = 'http://42.115.221.44:8080/devcamp-pizza365/orders';
    $.ajax({
      type: 'POST',
      url: vORDERS_URL,
      data: JSON.stringify(paramData),
      async: false,
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json',
      success: function (response) {
        $('#div-order-new').modal('hide');
        swal('Weldone!', 'Tạo đơn hàng thành công!', 'success').then(
          function () {
            sendRequestToGetAllOrder();
            setTimeout(function () {
              $('.loading-screen').fadeOut(300);
              loadDataToTable(gOrderDb.orders);
            }, 500);
          }
        );
      },
      error: function (error) {
        swal('Opps!', 'Something Went Wrong!', 'error').then(function () {
          sendRequestToGetAllOrder();
          setTimeout(function () {
            $('.loading-screen').fadeOut(300);
            loadDataToTable(gOrderDb.orders);
          }, 500);
        });
      },
    }).done(function () {
      resetInputAddNewModal();
    });
  }

  //khai bao ham thuc hien call api voucher ID
  function sendRequesetToGetVoucherCode(paramVoucherId) {
    'use strict';
    const vVOUCHER_URL =
      'http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/';

    let vVoucherObj = {
      id: 0,
      maVoucher: '',
      phanTramGiamGia: '',
    };
    $.ajax({
      type: 'GET',
      url: vVOUCHER_URL + paramVoucherId,
      async: false,
      dataType: 'json',
      success: function (response) {
        vVoucherObj.id = response.id;
        vVoucherObj.maVoucher = response.maVoucher;
        vVoucherObj.phanTramGiamGia = response.phanTramGiamGia;
      },
      error: function (error) {
        vVoucherObj.id = 0;
        vVoucherObj.maVoucher = '';
        vVoucherObj.phanTramGiamGia = '0';
        console.assert(error.responseText);
      },
    });
    return vVoucherObj;
  }

  // function call api order detail
  function sendParamToGetOrderDetail(paramOrderId) {
    'use strict';
    $.ajax({
      type: 'GET',
      url: gBASE_URL + '/' + paramOrderId,
      async: false,
      dataType: 'json',
      success: function (responseObject) {
        gOrderDb.order = responseObject;
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
    return gOrderDb.order;
  }

  // function call api update order
  function senDataToUpdateOrderInfo(paramStatus, paramId) {
    'use strict';
    let vORDER_URL = 'http://42.115.221.44:8080/devcamp-pizza365/orders';
    let vObjectRequest = {
      trangThai: paramStatus, //3 trang thai open, confirmed, cancel
    };
    gCurrentRowOnEdit.css({
      filter: 'contrast(0.5)',
      transition: '0.3s all linear',
    });
    $.ajax({
      type: 'PUT',
      url: vORDER_URL + '/' + paramId,
      data: JSON.stringify(vObjectRequest),
      async: false,
      contentType: 'application/json;charset=UTF-8',
      dataType: 'json',
      beforeSend: function () {
        $('.loading-screen').fadeIn(300);
      },
      success: function (response) {
        setTimeout(function () {
          $('.loading-screen').fadeOut(300);
          vAlert();
        }, 1000);

        let vAlert = () => {
          swal('Weldone!', 'Lưu dữ liệu thành công!', 'success');
        };
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  // khai bao ham xu ly drink option
  function sendRequestToGetDrinkOpts() {
    'use strict';
    let vDRINK_OPT_URL = 'http://42.115.221.44:8080/devcamp-pizza365/drinks';
    $.ajax({
      type: 'GET',
      url: vDRINK_OPT_URL,
      dataType: 'json',
      async: false,
      success: function (response) {
        gOrderDb.drinkOpt = response;
        //load drink opts
        renderOptionDrinks(gOrderDb.drinkOpt);
      },
      error: function (error) {
        console.assert(error.responseText);
      },
    });
  }

  // render option drinks
  function renderOptionDrinks(paramDrink) {
    'use strict';
    paramDrink.map((bI) => {
      $('#select-edit-drink').append(
        $('<option/>', {
          value: bI.maNuocUong,
          text: bI.tenNuocUong,
        })
      );
      $('#select-addnew-drink').append(
        $('<option/>', {
          value: bI.maNuocUong,
          text: bI.tenNuocUong,
        })
      );
    });
  }

  //function validate input value modal addnew
  function validate(paramOrder) {
    'use strict';
    let vOption = {
      animation: true,
      delay: 2000,
      autohide: true,
    };
    if (paramOrder.kichCo == '') {
      let vMsg = 'Bạn chưa chọn kích cỡ';
      createToastElement(vMsg);
      let vToastElement = $('.toast').toast(vOption);
      vToastElement.toast('show');
      return false;
    }
    if (paramOrder.hoTen == '') {
      let vMsg = 'Bạn chưa nhập họ và tên';
      createToastElement(vMsg);
      let vToastElement = $('.toast').toast(vOption);
      vToastElement.toast('show');
      return false;
    }
    if (paramOrder.email == '') {
      let vMsg = 'Bạn nên nhập email để tiện liên lạc';
      let vToastColor = {
        backGround: 'bg-warning',
        text: 'text-dark',
      };
      if (gOnce) {
        createToastElement(vMsg, vToastColor);
        let vToastElement = $('.toast').toast(vOption);
        vToastElement.toast('show');
      }
      gOnce = false;
    }
    if (paramOrder.soDienThoai == '') {
      let vMsg = 'Bạn chưa nhập số điện thoại';
      createToastElement(vMsg);
      let vToastElement = $('.toast').toast(vOption);
      vToastElement.toast('show');
      return false;
    }
    if (paramOrder.diaChi == '') {
      let vMsg = 'Bạn chưa nhập địa chỉ';
      createToastElement(vMsg);
      let vToastElement = $('.toast').toast(vOption);
      vToastElement.toast('show');
      return false;
    }
    if (paramOrder.loaiPizza == 'none') {
      let vMsg = 'Bạn chưa loại pizza';
      createToastElement(vMsg);
      let vToastElement = $('.toast').toast(vOption);
      vToastElement.toast('show');
      return false;
    }
    if (paramOrder.idLoaiNuocUong == 'none') {
      let vMsg = 'Bạn chưa chọn đồ uống';
      createToastElement(vMsg);
      let vToastElement = $('.toast').toast(vOption);
      vToastElement.toast('show');
      return false;
    }
    return true;
  }

  // hàm tạo toast thông báo
  function createToastElement(paramParagrap, paramCustom) {
    'use strict';
    let vToast = '';
    if (paramCustom != null) {
      vToast = `
      <div class="toast align-items-center ${paramCustom.text} ${paramCustom.backGround} border-0">
        <div class="toast-header ${paramCustom.backGround}">
          <strong class="me-auto">Thông báo</strong>
          <small class="${paramCustom.text}">just now</small>
          <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">${paramParagrap}</div>
      </div>`;
    } else {
      vToast = `
      <div class="toast align-items-center text-white bg-danger border-0">
        <div class="toast-header bg-danger">
          <strong class="me-auto">Thông báo</strong>
          <small class="text-light">just now</small>
          <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">${paramParagrap}</div>
      </div>`;
    }

    $('.toast-container').append(vToast);
  }

  // reset input new modal
  function resetInputAddNewModal() {
    'use strict';
    $('#input-addnew-voucher').val('');
    $('#select-addnew-drink').val('none').change();
    $('#select-addnew-pizza-type').val('none').change();
    $('#input-addnew-fullname').val('');
    $('#input-addnew-email').val('');
    $('#input-addnew-phone').val('');
    $('#input-addnew-address').val('');
    $('#input-addnew-message').val('');
  }
})(jQuery);
